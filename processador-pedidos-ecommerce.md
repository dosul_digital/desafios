# Processador de Pedidos para E-commerce

Você precisa desenvolver uma função em PHP para processar uma lista de pedidos de um e-commerce. Cada pedido contém:
- Um identificador único (`id`).
- O nome do cliente (`cliente`).
- A lista de produtos comprados (`produtos`), onde cada produto tem:
	- Nome (`nome`).
	- Preço unitário (`preco`).
	- Quantidade (`quantidade`).

A função deve receber um array de pedidos e retornar um relatório com:
1. O total de pedidos processados.
2. O valor total vendido.
3. O cliente que mais gastou.

**Exemplo de Entrada:**

    $pedidos = [
	    [
	        "id" => 1,
	        "cliente" => "João",
	        "produtos" => [
	            ["nome" => "Notebook", "preco" => 3500, "quantidade" => 1],
	            ["nome" => "Mouse", "preco" => 150, "quantidade" => 2]
	        ]
	    ],
	    [
	        "id" => 2,
	        "cliente" => "Maria",
	        "produtos" => [
	            ["nome" => "Teclado", "preco" => 300, "quantidade" => 1],
	            ["nome" => "Monitor", "preco" => 1200, "quantidade" => 1]
	        ]
	    ],
	    [
	        "id" => 3,
	        "cliente" => "João",
	        "produtos" => [
	            ["nome" => "Cadeira Gamer", "preco" => 900, "quantidade" => 1]
	        ]
	    ],
	    [
	        "id" => 4,
	        "cliente" => "Carlos",
	        "produtos" => [
	            ["nome" => "Impressora", "preco" => 800, "quantidade" => 1],
	            ["nome" => "Scanner", "preco" => 600, "quantidade" => 1]
	        ]
	    ],
	    [
	        "id" => 5,
	        "cliente" => "Maria",
	        "produtos" => [
	            ["nome" => "Smartphone", "preco" => 2500, "quantidade" => 1],
	            ["nome" => "Fone de ouvido", "preco" => 200, "quantidade" => 2]
	        ]
	    ],
	    [
	        "id" => 6,
	        "cliente" => "João",
	        "produtos" => [
	            ["nome" => "Tablet", "preco" => 1200, "quantidade" => 1],
	            ["nome" => "Capinha", "preco" => 50, "quantidade" => 3]
	        ]
	    ],
	    [
	        "id" => 7,
	        "cliente" => "Ana",
	        "produtos" => [
	            ["nome" => "Smartwatch", "preco" => 1800, "quantidade" => 1],
	            ["nome" => "Carregador", "preco" => 150, "quantidade" => 1]
	        ]
	    ],
	    [
	        "id" => 8,
	        "cliente" => "Carlos",
	        "produtos" => [
	            ["nome" => "Câmera", "preco" => 2200, "quantidade" => 1],
	            ["nome" => "Tripé", "preco" => 400, "quantidade" => 2]
	        ]
	    ]
    ];


**Saída Esperada:**

    [
	    "total_pedidos" => 8,
	    "total_vendido" => 16800,
	    "cliente_mais_gastou" => "João"
    ]

**Requisitos:**
- A função deve calcular corretamente o valor total de cada pedido multiplicando **preço × quantidade**.
- O cálculo do cliente que mais gastou deve ser feito somando todos os pedidos dele.
- O código deve ser bem estruturado e eficiente.

### Critérios de Avaliação

**Correção:**
- A função deve retornar os resultados corretos de acordo com os requisitos especificados.

**Eficiência:**
- O algoritmo deve ser eficiente para entradas de tamanho moderado.