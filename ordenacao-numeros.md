# Desafio Ordenação e Filtragem de Números

Crie uma função em PHP que receba um array de números inteiros e realize as seguintes operações:

#### Requisitos:
- A função deve receber um array de números inteiros (pode conter números positivos e negativos).
- Filtrar o array para remover todos os números negativos.
- Ordenar o array filtrado em ordem crescente.
- Retornar o array resultante.
- Se o array original não contiver números positivos, a função deve retornar uma mensagem informando que não há números positivos disponíveis.

#### Exemplo de uso da função:

```
<?php
$numeros = [3, -1, 5, 0, -7, 2, -3];
$resultado = filtraEOrdenaNumeros($numeros);
print_r($resultado); // Deve retornar [0, 2, 3, 5]

$numeros2 = [-5, -10, -1];
$resultado2 = filtraEOrdenaNumeros($numeros2);
echo $resultado2; // Deve retornar "Não há números positivos disponíveis."
```