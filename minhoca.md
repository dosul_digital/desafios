# Desafio da minhoca 🪱

Crie uma função que receba uma string como parâmetro, e a saída seja a mesma string mas com cada exibição uma letra como maiúscula.

Com a string **crédito** a saída esperada é:
```
Crédito
cRédito
crÉdito
créDito
crédIto
crédiTo
créditO
```

Preferencialmente resolva este problema utilizando Javascript ou PHP

### Sugestão:
Suba seu código no [Replit](https://replit.com/) e compartilhe com o e-mail **trabalhecom@dscomm.com.br**
