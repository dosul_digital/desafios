# Manipulação de Strings e Arrays

**Objetivo:** Criar uma função em PHP que receba uma string como entrada, conte a frequência de cada caractere, e retorne os caracteres em ordem decrescente de frequência. Em caso de empate, os caracteres devem ser ordenados em ordem alfabética.

### Detalhes do Teste

**Entrada:**
 - Uma string composta por letras (maiúsculas e minúsculas) e espaços.

**Saída:**
- Um array associativo onde as chaves são os caracteres e os valores são suas respectivas frequências, ordenados de acordo com as regras mencionadas.

**Exemplo:**
- Entrada: "banana apple"
- Saída: ['a' => 4, 'n' => 2, 'p' => 2, 'b' => 1, 'e' => 1, 'l' => 1, ' ' => 1]


### Regras:
- Ignore a diferença entre maiúsculas e minúsculas (trate tudo como minúsculo).
- Considere espaços como caracteres válidos.
- Em caso de empate na frequência, ordene os caracteres em ordem alfabética.

### Importante:
- Evite usar inteligência artificial para resolver este desafio. O objetivo é testar sua habilidade em lógica de programação e compreensão da linguagem PHP.


### Critérios de Avaliação

**Correção:**
- A função deve retornar os resultados corretos de acordo com os requisitos especificados.

**Simplicidade e Clareza:**
- O código deve ser simples, claro e bem comentado.

**Eficiência:**
- O algoritmo deve ser eficiente para entradas de tamanho moderado.

## Entrega
- Entregar o código fonte em um repositório Git (público ou privado com acesso compartilhado). Contendo um arquivo .php com o código da função e os testes realizados.
